package com.example.duoc.practicaclase3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class IngresoActivity extends AppCompatActivity {

    TextView lblUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingreso);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        lblUsuario = ((TextView) findViewById(R.id.lblUsuario));
        if (bundle != null){
            lblUsuario.setText(bundle.get("usuario").toString());
        }
    }
}
