package com.example.duoc.practicaclase3;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    EditText txtUser, txtPass;
    Button btnIngresar, btnNuevoUsuario;
    ImageView logo_android;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtUser = (EditText)findViewById(R.id.txtUser);
        txtPass = (EditText)findViewById(R.id.txtPass);
        btnIngresar = (Button)findViewById(R.id.btnIngresar);
        btnNuevoUsuario = (Button)findViewById(R.id.btnNuevoUsuario);
        logo_android = ((ImageView) findViewById(R.id.logo_android));

        logo_android.setOnClickListener(this);

        btnIngresar.setOnClickListener(this);
        btnNuevoUsuario.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnIngresar:
                if (txtUser.getText().toString().equals("admin") && txtPass.getText().toString().equals("admin")){
                    Toast.makeText(this, "Ok", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(this, IngresoActivity.class);
                    intent.putExtra("usuario", txtUser.getText().toString());
                    startActivity(intent);
                }else{
                    Toast.makeText(this, "Usuario o Password incorrectos", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btnNuevoUsuario:
                Toast.makeText(this, txtUser.getText().toString(), Toast.LENGTH_LONG).show();
                break;
            case R.id.logo_android:
                Toast.makeText(this, "Logo", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
